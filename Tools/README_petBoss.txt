Usage:

./petBoss.exe -b < *.eqn 
(it prints the extracted input signature when all the equations are rewritted.)

eqn format:
    Each equation represents one gate in the verilog file.
    The output signature must be written in the bottom of the file.
    (You have to write a passer to translate verilog into eqn.)