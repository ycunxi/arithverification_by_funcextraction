We have uploaded our combinational arithmetic benchmarks to this repository. These benchmarks were used in our DAC15 paper. Right now, I have uploaded Verilog and Blif (part of benchmarks). I will upload AIGER in the future.

If you have question, please contact ycunxi@umass.edu (Cunxi Yu).

Reference: 

[C1] Maciej Ciesielski, Cunxi Yu, Walter Brown, Duo Liu, "Verification of Gate-level Arithmetic Circuits by Function Extraction." Design Automation Conference (DAC'15)

[J1] Cunxi Yu, Walter Brown, Duo Liu, André Rossi, Maciej J. Ciesielski: Formal Verification of Arithmetic Circuits by Function Extraction. IEEE Trans. on CAD of Integrated Circuits and Systems 35(12): 2131-2142 (2016)

University of Massachusetts,Amherst
VLSI CAD (Advisor : Prof.Ciesielski)

Updated on June 2016